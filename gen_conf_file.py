import os
import yaml

region = os.getenv('AWS_REGION')
key = os.getenv('AWS_ACCESS_KEY_ID')
secret = os.getenv('AWS_SECRET_ACCESS_KEY')
backend_bucket = os.getenv('AWS_BACKEND_BUCKET')
key_name = os.getenv('AWS_KEYPAIR_NAME')
key_path = os.getenv('AWS_KEYPAIR_PATH')

yaml_string = '''{
"region":"%s" ,
"key_id":"%s" ,
"secret_key":"%s",
"key_name":"%s",
"key_path":"%s",
"backend_bucket":"%s"
}
'''%(region, key, secret, key_name, key_path, backend_bucket)

with open('dist_conf_self.yml','w') as out:
    out.write(yaml_string)

sh_string=open('bootstrap.sh','r').read().format(**{
                                    'REGION':region,
                                    'KEY':key,
                                    'SECRET':secret,
                                    'BBUCKET':backend_bucket})

with open('bootstrap_self.sh','w') as out:
    out.write(sh_string)
