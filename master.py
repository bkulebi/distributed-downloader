import boto.ec2, boto.s3
import os
import yaml
import time

from boto.manage.cmdshell import sshclient_from_instance

class AWSControl(object):
    def __init__(self,aws_credentials=None,conf_file_path=None,no_of_ec2=None):
        if type(aws_credentials) != dict:
            raise TypeError('aws_credentials need to be a string')
        aws_keys = ['region','key_id','secret_key']
        for aws_key in aws_keys:
            if not aws_credentials.get(aws_key):
                raise ValueError('the aws_credential %s does not exist'\
                                                                      %aws_key)
        self.conf_file_path = conf_file_path
        self.region = aws_credentials.get('region')
        self.key_id = aws_credentials.get('key_id')
        self.secret_key = aws_credentials.get('secret_key')
        self.key_name = aws_credentials.get('key_name')
        self.key_path = aws_credentials.get('key_path')
        self.security_g = aws_credentials.get('security_g') or 'default'

        if not os.path.exists(conf_file_path):
            raise IOError('configuration file %s does not exist'
                                                          %self.conf_file_path)
        self.configuration = self.get_configuration_parameters()

        if not self.configuration.get('instances'):
            self.no_instances = int(no_of_ec2)
        else:
            if not self.configuration['instances'].get('instance_ids'):
                raise ValueError("instance ids do not exist in the"\
                                 " configuration file %s"%self.conf_file_path)
            self.instance_ids = self.configuration['instances']['instance_ids']
            self.no_instances = int(self.configuration['instances']['no'])

    def get_configuration_parameters(self):
        return yaml.load(open(self.conf_file_path,'r'))

    def initialize_ec2(self):
        print('Connecting to AWS region to initialize instances')
        self.ec2_conn = boto.ec2.connect_to_region(self.region,
                                          aws_access_key_id=self.key_id,
                                          aws_secret_access_key=self.secret_key)
        print('Running %i instances:'%self.no_instances)
        instance_ids = []
        for i in range(self.no_instances):
            reservation = self.ec2_conn.run_instances('ami-785db401',# ubuntu
                                             key_name=self.key_name,
                                             instance_type='t2.micro',
                                             security_groups=[self.security_g])
            print(reservation.instances[0].id)
            instance_ids.append(reservation.instances[0].id)

        not_ready = True
        instances_running = []
        while(not_ready):
            time.sleep(6)
            bools = []
            reservations = self.ec2_conn.get_all_instances()
            for reservation in reservations:
                current_instance = reservation.instances[0]
                if current_instance.id in instance_ids:
                    state = current_instance.state
                    print('instance %s is %s'%(current_instance.id,state))
                    if state == 'running':
                        instances_running.append(current_instance.id)
            if set(instances_running) == set(instance_ids):
                not_ready = False

        self.instance_ids = instance_ids
        self.configuration['instances'] = {'no':self.no_instances,
                                           'instance_ids':instance_ids}
        with open(self.conf_file_path,'w') as out:
            yaml.dump(self.configuration, out)
        print('configuration file rewritten')

        return True

    def setup_workers(self):
        print('setting up the instances')
        self.ec2_conn = boto.ec2.connect_to_region(self.region,
                                          aws_access_key_id=self.key_id,
                                          aws_secret_access_key=self.secret_key)
           
        boot_file = 'bootstrap_self.sh'
        if not os.path.exists(boot_file):
            raise IOError('%s need to setup workers'%boot_file)

        reservations = self.ec2_conn.get_all_instances()
        for reservation in reservations:
            current_instance = reservation.instances[0]
            if current_instance.id in self.instance_ids:
                if current_instance.state == 'running':
                    self.bootstrap(current_instance)
                    print('celery workers should be running in %s'%current_instance.id)
                else:
                    raise ValueError('instance found, but not running. %s'\
                                     %current_instance.state)

    def bootstrap(self,instance):
        print('bootstrapping instance:%s'%instance.id)
        ssh_client = sshclient_from_instance(instance,
                                             self.key_path,
                                             user_name='ubuntu')
        ssh_client.put_file('bootstrap_self.sh','bootstrap_self.sh')
        status, stdout, stderr = ssh_client.run('source bootstrap_self.sh') 
        stdout_str = stdout.decode('utf8')
        stderr_str = stderr.decode('utf8')
        if stdout_str.split('\n')[-2] != '1':
            raise RuntimeError('bootstrap script failed.\n%s'%stdout_str)
        print(stderr_str)

        print('deploying workers')
        ssh_client.put_file('dist_conf_self.yml','distributed-downloader/dist_conf_self.yml')
        status, stdout, stderr = ssh_client.run('screen -dmS worker bash distributed-downloader/run_worker.sh')
        return True

    def initialize_s3(self):
        pass

    def remove_ec2(self):
        print('Connecting to AWS region to terminate instances')
        conn = boto.ec2.connect_to_region(self.region,
                                          aws_access_key_id=self.key_id,
                                          aws_secret_access_key=self.secret_key)
       
        if not self.configuration.get('instances'):
            raise ValueError('Configuration file does not have instances info.')

        instance_ids = self.configuration['instances']['instance_ids']
        reservations = conn.get_all_instances()
        aws_instance_ids = []
        for reservation in reservations:
            if reservation.instances[0].state == 'running':
                aws_instance_ids.append(reservation.instances[0].id)
        if not set(instance_ids).issubset(set(aws_instance_ids)):
            message = 'the persisted instances are not in actual ec2 instance'\
                      ' list:\ninstance_ids:%s\nec2_ids:%s'\
                      %(','.join(instance_ids),','.join(aws_instance_ids))
            raise ValueError(message)
        else:
            print('Instance ids found. Terminating')
            conn.terminate_instances(instance_ids=instance_ids)

        return True

    def remove_s3(self):
        pass
