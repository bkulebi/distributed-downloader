#!/bin/bash
sudo apt update
sudo apt install -y libssl-dev libcurl4-openssl-dev python-dev python3-pip screen
pip3 install --upgrade pip
export LC_ALL="en_US.UTF-8"
export AWS_REGION='{REGION}'
export AWS_ACCESS_KEY_ID='{KEY}'
export AWS_SECRET_ACCESS_KEY='{SECRET}'
export AWS_BACKEND_BUCKET='{BBUCKET}'
echo ''' 
export LC_ALL="en_US.UTF-8"
export AWS_REGION="{REGION}"
export AWS_ACCESS_KEY_ID="{KEY}"
export AWS_SECRET_ACCESS_KEY="{SECRET}"
export AWS_BACKEND_BUCKET="{BBUCKET}"
''' >> ~/.bashrc
git clone https://bitbucket.org/bkulebi/distributed-downloader
sudo pip3 install -r distributed-downloader/requirements.txt
echo '1'
