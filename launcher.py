from celery import Celery
import master
import os
import time
import yaml

from optparse import OptionParser
from six.moves.urllib.parse import quote

aws_credentials = yaml.load(open('dist_conf_self.yml','r'))
sqs_broker = 'sqs://%s:%s@'%(quote(aws_credentials['key_id']),
                             quote(aws_credentials['secret_key']))

app = Celery('launcher',broker=sqs_broker)

app.conf.update(CELERY_RESULT_BACKEND = 'celery_s3.backends.S3Backend',
                CELERY_S3_BACKEND_SETTINGS = {
                                           'aws_access_key_id': aws_credentials['key_id'],
                                           'aws_secret_access_key': aws_credentials['secret_key'],
                                           'bucket': aws_credentials['backend_bucket']})

def main(options):
    if options.task == 'run':
        initialize(options)
        result = add.delay(4,4)
        print(result.get())
        if result.get() == 8:
            print('workers seem to be online')
    elif options.task == 'clean':
        terminate(options)
    else:
        print('%s task not recognized, and should not be here'%options.task)

def initialize(options):
    if not options.no_of_ec2:
        raise ValueError("number of instances needed for the setup."\
                         "\nfor help: python3 %s --help"%__file__)
    print('initializing instances')
    aws = master.AWSControl(aws_credentials=aws_credentials,
                            conf_file_path=options.conf_file_path,
                            no_of_ec2=options.no_of_ec2)
    aws.initialize_ec2()
    aws.setup_workers()

def terminate(options):
    print('terminating instances')
    aws = master.AWSControl(aws_credentials=aws_credentials,
                            conf_file_path=options.conf_file_path,
                            no_of_ec2=options.no_of_ec2)
    aws.remove_ec2()

@app.task
def add(x,y):
    time.sleep(5)
    return x+y

if __name__ == "__main__":
    tasks = ["run","clean"]
    usage = "usage: %prog [-t task] [option]"
    parser = OptionParser(usage=usage)
    parser.add_option("-n","--no_of_ec2",dest="no_of_ec2",default=None,
                       help="number of ec2 instances to be used",
                       type="int")
    parser.add_option("-b","--bucket",dest="s3_bucket_name",default=None,
                      help="s3 bucket name to upload the resources",
                      type="string")
    parser.add_option("-c","--configuration",dest="conf_file_path",
                      default=None,
                      help="path of the configuration file dist_conf_self.yml",
                      type="string")
    parser.add_option("-t","--task",dest="task",
                      default="run",
                      help="the task to execute: Could be %s"%','.join(tasks),
                      type="string")

    (options, args) = parser.parse_args()
    if options.task not in tasks:
        raise ValueError("give task '%s' not in list of possible tasks\n%s "
                         %(options.task,','.join(tasks)))

    main(options)
