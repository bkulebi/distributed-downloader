# Distributed (ftp) downloader

Parallelizes a complete download of a public ftp via AWS tools and celery. 

Only the celery+AWS setup is working and the ftp download is not implemented yet. Currently the script sets up a given number of EC2 instances, configures the celery instances using the AWS SQS as a broker and an S3 bucket as backend. Finally deploys and runs the workers in the EC2 instances. 

Currently the configuration is not yet automatized, most importantly a queue needs to be created manually via SQS. Additionally the AWS credentials additional information needs to be put as environment variables in the environment. For bash systems the `~/.profile` can be appended as:

```
export AWS_REGION=<region>
export AWS_ACCESS_KEY_ID='XXXXXXXXXXXXXXXXXXXX'
export AWS_SECRET_ACCESS_KEY='XXXXXXxxXXXxXxXXXXXxxxxxxXxXXXXXXXXXxxxx'
export AWS_BACKEND_BUCKET=<bucket-for-celery>
export AWS_KEYPAIR_NAME=<aws-keypairs-to-be-used>
export AWS_KEYPAIR_PATH=<aws-keypair-file-path>
```

To use the script, simply install celery and the pip requirements
```
sudo apt-get install celeryd
pip3 install -r requirements.txt
```

## run program
In order to deploy the celery workers, the aws credentials need to be copied to the EC2 instances. Before running the code these files need to be generated. 
```
python3 gen_conf_file.py
```
Based on the templates the `bootstrap_self.sh` and `dist_conf_self.yml` are generated. To run the task simply
```
python3 -n <number of EC2 instances> -c dist_conf_self.yml
```
or
```
python3 -t run -n <number of EC2 instances> -c dist_conf_self.yml
```


`dist_conf_self.yml` keeps information on the EC2 instance ids (and the task progress in the future), hence it can be used for terminating the EC2 instances when the tasks are finished.

```
python3 -t clean -c dist_conf_self.yml
```

For each new run `gen_conf_file.py` needs to be run again, in order to ''clean'' the `dist_conf_self.yml`, because of the redundancy between the number of instances coming from `-n` and from the file itself.  

## launch workers locally
The celery command that is used, not necessary for the code to function.
```
celery -A launcher worker --loglevel=info
```
